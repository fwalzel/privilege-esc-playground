/**
 * @package: wp-form-collect
 * version: 1.0.2
 *
 * author: Florian Walzel
 * date: 2018-04-25
 * licence: MIT
 */

'use strict';

(function($) {

    /**
     *
     * @param elem
     */
    var teleprompt = function (elem) {
        this.$el = $(elem);
        this.txt = this.$el.html(),

        // defaults
        this.delayMin = typeof this.$el.attr('data-delay-min') !== 'undefined' ? parseInt(this.$el.attr('data-delay-min')) : 45,
        this.delayMax = typeof this.$el.attr('data-delay-max') !== 'undefined' ? parseInt(this.$el.attr('data-delay-max')) : 70,
        this.fixArr = typeof this.$el.attr('data-fixwords') !== 'undefined' ? this.$el.attr('data-fixwords').split(',') : [];

        this.$el.empty();
        // container wit attribute [data-teleprompt] are by default hidden
        this.$el.css('visibility','visible');
    };

    /**
     *
     * @param min
     * @param max
     * @returns {*}
     */
    teleprompt.prototype.rand = function (min, max) {
        var min = min || this.delayMin,
            max = max || this.delayMax;

        return (min == max) ? min : Math.floor(Math.random() * (max - min + 1) + min);
    };

    teleprompt.prototype.scrollCheck = function(el) {
        var $el = $(el),
            offset = $el.offset(),
            deltaH = offset.top - window.innerHeight * .9;
            //console.log(deltaH);
        if (deltaH > 0) {
            //console.log($el.parent());
            $el.parent().scrollTop(deltaH+50);
        }

    };

    teleprompt.prototype.start = function (whenDone, scroll) {

        this.whenDone = whenDone || function(){};
        this.scroll = scroll;

        /**
         * 
         */
        (function prompt(ctx, i, replace, preserve, fixNr, fixIndex) {
            setTimeout(function () {
                
                var elem = ctx.$el,
                    txt = ctx.txt,
                    fix = ctx.fixArr[fixNr];

                if (typeof ctx.scroll === 'string')
                    ctx.scrollCheck(scroll);

                if (txt[i] == '§') {
                    replace = 'backward';
                    preserve = i+1;
                }

                if (replace == 'backward' && txt[i-1] == ' ') {
                    replace = 'replace';
                }

                if (replace == 'replace' && fixIndex == fix.length) {
                    replace = 'forward';
                    i = preserve; // continue at right place of index
                    fixIndex = 0; // reset the index
                    if (ctx.fixArr.length >= fixNr+1)
                        fixNr += 1; // move to next fix word
                }

                if (replace == 'forward') {
                    // slice the last element which is the cursor character and add current letter + new cursor
                    elem.html( elem.html().slice(0, elem.html().length-1) + txt[i] + '|' );
                    // the recursive call
                    if (i<txt.length-1) {
                        prompt(ctx, i + 1, 'forward', preserve, fixNr, fixIndex);
                    }
                    else {
                        // remove cursor at end
                        // console.log('at end');
                        elem.html( elem.html().slice(0, elem.html().length-1) );
                        setTimeout(function() {
                                ctx.whenDone()
                            }, 1000
                        );
                    }
                }
                else if (replace == 'backward') {
                    // shorten the string from back
                    elem.html( elem.html().slice(0, elem.html().length-2) + '|' );
                    // the recursive call
                    prompt(ctx, i-1, 'backward', preserve, fixNr, fixIndex);
                }
                else if (replace == 'replace') {
                    elem.html( elem.html().slice(0, elem.html().length-1) + fix[fixIndex] + '|' );
                    prompt(ctx, i, 'replace', preserve, fixNr, fixIndex+1);
                }
                
            }, ctx.rand() );
        })(this, 0, 'forward', 0, 0, 0);
    };

    /**
     * Export everything to the window object, so that it is globally available
     */
    if (typeof window.WpForms === 'undefined')
        window.Teleprompt = teleprompt;
    else
        console.log('@ Teleprompt could not be constructed, a variable with that name already exist.');

})(jQuery);

