/**
 * Created by florianwalzel on 23.07.18.
 */

$(document).ready(function() {

    var $m1 = $('#main-1');
    var $m2 = $('#main-2');
    var $m3 = $('#main-3');

    var $right = $('.right');

    /* SHEET 1
    -------------------------------- */

    prompt1 = new Teleprompt('#parag-1-1');
    prompt2 = new Teleprompt('#parag-1-2');
    prompt3 = new Teleprompt('#parag-1-3');
    prompt4 = new Teleprompt('#parag-1-4');

    prompt5 = new Teleprompt('#parag-2-1');
    prompt6 = new Teleprompt('#parag-2-2');

    prompt7 = new Teleprompt('#parag-3-1');

    prompt1.start(
        function() { prompt2.start(
            function() { prompt3.start(
                function() { prompt4.start(
                    function() {
                        setTimeout(function () {
                            $m1.addClass('opc-0')
                        }, 3000);
                    }, '#m1-end')
                })
            })
        }
    );

    /* SHEET 2
     -------------------------------- */

    $m1.on('transitionend webkitTransitionEnd',
        function(e){
            $(this).off(e);
            $(this).remove();
            $m2.removeClass('no-disp').addClass('opc-1');
            setTimeout(function(){ prompt5.start(
                function() {
                    setTimeout(function(){ $right.addClass('opc-1') }, 1200);
                    setTimeout(function(){ $('#r-1').addClass('opc-0') }, 2000+1200);
                    setTimeout(function(){ $('#r-2').addClass('opc-0') }, 4000+1200);
                    setTimeout(function(){ $('#r-3').addClass('opc-0') }, 6000+1200);
                    setTimeout(function(){ $('#r-4').addClass('opc-0') }, 8000+1200);
                    setTimeout(function(){ $('#r-5').addClass('opc-0') }, 10000+1200);
                    setTimeout(function(){ $('#r-6').addClass('opc-0') }, 12000+1200);
                    setTimeout(function(){ $('#r-7').addClass('opc-0') }, 14000+1200);

                    setTimeout(function(){ prompt6.start(
                        function() {
                            setTimeout(function () {
                                $m2.removeClass('opc-1');
                                setTimeout(function () {
                                    $m2.remove();
                                    $m3.removeClass('no-disp').addClass('opc-1');
                                    prompt7.start(
                                        function() {
                                            setTimeout(function() { window.location.reload(false); }, 4000);
                                        }
                                    );
                                }, 1000)
                            }, 2400)
                        })
                    }, 15000+1200);
                }
            ) }, 1200);
        });
});